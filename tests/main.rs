#[cfg(test)]
mod tests {
    use cli_test_dir::*;

    const BIN: &'static str = "./main";

    #[test]
    fn pattern1() {
        let test_dir = TestDir::new(BIN, "");
        let output = test_dir
            .cmd()
            .output_with_stdin(r#"15
10
"#)
            .tee_output()
            .expect_success();
        assert_eq!(output.stdout_str(), "5\n");
        assert!(output.stderr_str().is_empty());
    }

    #[test]
    fn pattern2() {
        let test_dir = TestDir::new(BIN, "");
        let output = test_dir
            .cmd()
            .output_with_stdin(r#"0
0
"#)
            .tee_output()
            .expect_success();
        assert_eq!(output.stdout_str(), "0\n");
        assert!(output.stderr_str().is_empty());
    }

    #[test]
    fn pattern3() {
        let test_dir = TestDir::new(BIN, "");
        let output = test_dir
            .cmd()
            .output_with_stdin(r#"5
20
"#)
            .tee_output()
            .expect_success();
        assert_eq!(output.stdout_str(), "-15\n");
        assert!(output.stderr_str().is_empty());
    }
}