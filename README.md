# AtCoder Template for cargo-generate

# Usage

1. create project
```shell
cargo generate --git https://gitlab.com/okutom/atcoder-template
```
2. edit src/main.rs
3. edit tests/main.rs
4. test
```shell
cargo test
```
5. submit